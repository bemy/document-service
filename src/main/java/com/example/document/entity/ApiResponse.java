package com.example.document.entity;

import com.example.document.enumerate.ReturnEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
    private String code;
    private String message;
    private Object data;

    public ApiResponse code(String code) {
        this.code = code;
        return this;
    }

    public ApiResponse message(String message) {
        this.message = message;
        return this;
    }

    public ApiResponse data(Object data) {
        this.data = data;
        return this;
    }

    public static ApiResponse success(String code, String message, Object data) {
        return new ApiResponse(code, message, data);
    }

    public static ApiResponse success(String message, Object data) {
        return success(ReturnEnum.SUCCESS.getCode(), message, data);
    }

    public static ApiResponse success(Object data) {
        return success(ReturnEnum.SUCCESS.getCode(), ReturnEnum.SUCCESS.getMsg(), data);
    }

    public static ApiResponse success(String message) {
        return success(ReturnEnum.SUCCESS.getCode(), message, null);
    }

    public static ApiResponse success() {
        return success(ReturnEnum.SUCCESS.getCode(), ReturnEnum.SUCCESS.getMsg(), null);
    }

    public static ApiResponse error(String code, String message) {
        return new ApiResponse().code(code).message(message);
    }

    public static ApiResponse error(String message) {
        return error(ReturnEnum.FAIL.getCode(), message);
    }
}