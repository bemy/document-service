package com.example.document.enumerate;

public enum ReturnEnum {
    SUCCESS("0000", "请求成功！"),
    FAIL("9999", "请求失败！"),
    UNKNOWN_ERROR("-1", "未知错误！"),
    A_ILLEGAL_PARAMETER("1001", "参数错误！"),
    A_ILLEGAL_METHOD("1002", "方法错误！"),
    /**
     * 一级错误码：用户相关
     */
    A_USER_CLIENT_ERROR("2000", "用户端错误！"),
    /**
     * 二级错误码：用户相关
     */
    A_USER_LOGIN_FAIL("2001", "用户登录失败！"),
    A_USER_NOT_EXIT("2002", "用户不存在！"),
    /**
     * 一级错误码：三方服务相关
     */
    B_3RD_ERROR("3000", "三方服务错误！"),
    /**
     * 二级错误码：请求相关
     */
    B_TIMEOUT("3001", "请求超时！"),
    B_WECHAT_OAUTH_ERROR("3002", "微信认证失败！"),
    B_GITHUB_OAUTH_ERROR("3003", "github认证失败！");

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    ReturnEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
