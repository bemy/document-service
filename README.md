# document-service

#### 介绍
文档服务

#### 软件架构
springcloud 2021.1

nacos + spring boot admin + minio


#### 安装教程

1.  启动minio

2.  启动nacos

3.  配置nacos

Data ID：documentService.properties

GOURP: PUBLIC_GROUP 

配置格式： properties

配置内容：
```
cn.minio.endpoint: http://127.0.0.1:9000
cn.minio.accessKey: C76TMYQ8G4A8M3EJ1YO3
cn.minio.secretKey: GYqDaA+IpEDYRXhzlklrms1cn8kCYEJZ4GWzwkqj
# 默认桶名称：每个系统可以建一个桶，存放自己的文件
cn.minio.bucketName: cnsf
```
4. 启动服务

依赖：
 https://gitee.com/bemy/minio-spring-boot-starter

#### 使用说明

1.  cn.minio.endpoint为minio的服务地址
2.  cn.minio.accessKey为service account里的Access Key
3.  cn.minio.secretKey为service account里的Secret Key

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
